(defproject http-demo "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [metosin/compojure-api "2.0.0-alpha31"]
                 [aleph "0.4.6"]
                 [ring "1.9.5"]
                 [ring-cors "0.1.13"]
                 [mount "0.1.16"]
                 [com.taoensso/timbre "5.1.2"]]
  :profiles  {:uberjar {:aot [http-demo.core]
                        :main ^:skip-aot http-demo.core}
              :dev {:plugins [[lein-ring "0.12.5"]]
                    :dependencies [[alembic "0.3.2"]]}}
  :ring {:handler http-demo.core/route
         :port 8080}
  ;; lein ring server # to run the server for development
  :repl-options {:init-ns http-demo.core})

