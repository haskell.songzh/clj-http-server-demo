(ns http-demo.core
  (:require
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.util.http-response :refer :all]
            [compojure.api.sweet :refer :all]
            [aleph.http :as http]
            [ring.util.http-response :refer :all]
            [schema.core :as s :refer [defschema]]
            [mount.core :as mount]
            [taoensso.timbre :as tm]
            [mount.core :refer [defstate start] :as mount]))

(defschema SignUpInfo
  {
   :user-name     (describe s/Str "user name")
   :user-nickname (describe s/Str "user nick name")
   :user-password (describe s/Str "user password")
   :user-phone    (describe s/Str "user phone number")
   :verification-code (describe s/Str "6 digits verification code")
   })

(defschema SignUpRes
  {:msg   (describe s/Str "message")
   :user-id (describe s/Int "user id")
   :user-name (describe s/Str "message")
   :user-role (describe s/Int "role")
   :user-level (describe s/Int "level")
   :user-status (describe s/Int "status")
   :token (describe s/Str "json web token")}
  )

(defn print-request [handler]
  (fn [req]
    (print req)
    (handler req)))

(def user-sign-up
  (POST "/sign-up" []
    :body [info SignUpInfo]
    :middleware [#(print-request %)]
    :responses {200 {:description "ok"}
                400 {:description "bad request"}
                500 {:description "database error"}}
    :summary "handle user sign up request"
    :return SignUpRes
    (ok {:msg   "haha"
         :user-id 1232456
         :user-name "song"
         :user-role 0
         :user-level 0
         :user-status 0
         :token "thisisatoken"})
    ))

(def route
  (api
    {:swagger
     {:ui   "/swagger-ui"
      :spec "/swagger.json"
      :data {:info {:title       "Sample API"
                    :description "Compojure Api example"}
             :tags [{:name "api", :description "some apis"}]}}}
    (context "" req
      :tags ["root"]
      (GET "/" []
        (do
          (with-out-str (clojure.pprint/pprint req))
          (ok {:msg "hello"})))
      user-sign-up
      (ANY "*" [] (not-found)))))

(defstate service
  :start
  (let [http-service (http/start-server (-> route
                                          (wrap-cors
                                            :access-control-allow-credentials
                                            true
                                            :access-control-allow-origin
                                            [#".*"]
                                            :access-control-allow-methods
                                            [:get :put :post :delete]
                                            :access-control-allow-headers
                                            ["Content-Type" "Accept"
                                             "Cache-Control" "Origin"
                                             "User-Agent" "Authorization"]))
                       {:port 8080})]
    (tm/info  "HTTP started")
    http-service)
  :stop
  ;; TODO need to refresh related caches
  (.close service))

(defn -main []
  (mount/start (-> (mount/find-all-states))))


